# vma-backend-test

**Backend test**
**VerifyMyAge Backend test**

Welcome candidate! Thanks for you interesting in being part of our team.
We have a simple challenge for you.
Please, create a REST API for a CRUD of users.
Our PO told us that we have to keep an record of:

- name
- age
- email
- password
- address



**Deliverables**

Full CRUD endpoints using Node.js and MongoDB or MySQL.
Fork and send us your repository link when ready.

**What we're looking for?**

Get to know your communication through code better.
Robustness, scalability and performance.
Security.

